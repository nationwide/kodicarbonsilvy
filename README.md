Customizing this theme for a specific application

Original Theme can be found here: https://github.com/idorel77/skin.CarPC-touch_carbon

Application:
-2003 Duramax Silverado Car Computer
-Frontend: 7in 1024x600 touchscreen
-Backend: Raspberry Pi 3 with 2 MCP23017 expanders for more pins
-Custom Functionality: 
-Use custom window to toggle pins on the Pi
-Pins to be used for things like High Idle, Auxiliary Lights, etc.